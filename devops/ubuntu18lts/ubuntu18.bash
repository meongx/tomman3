#!/usr/bin/env bash

export DEBIAN_FRONTEND=noninteractive

# Force Locale
echo "LC_ALL=en_US.UTF-8" | sudo tee -a /etc/default/locale
sudo locale-gen en_US.UTF-8

#sudo apt-get -qq update

echo "Installing Docker..."
curl -sSL https://get.docker.com/ | sudo sh > /dev/null
sudo usermod -aG docker $USER

echo "Installing Docker Compose..."
sudo curl -sSL "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

echo "done"
