v3

start map
```
docker run --rm -it -v $(pwd)/server/map:/mnt -p 80:80 klokantech/tileserver-gl --config /mnt/config.json
```

start gateway
```
docker run --rm -it -p 80:80 nginx:1.16-alpine
```

open shell on gateway
```
docker run -it --rm nginx:1.16-alpine /bin/ash
```
